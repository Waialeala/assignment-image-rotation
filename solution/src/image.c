#include "image.h"

#include <inttypes.h>
#include <malloc.h>

#include "pixel.h"

struct image init_image (uint32_t width, uint32_t height){
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc (width * height * sizeof (struct pixel));
    return image;
}

void free_image (struct image* image) {
    free(image->data);
}

bool set_pixel (struct image* image, struct pixel pixel, uint32_t x, uint32_t y){
    if (x < image->width && y < image->height) {
        *(image->data + x + y * image->width) = pixel;
        return true;
    }
    return false;
}

struct pixel get_pixel (const struct image* image, uint32_t x, uint32_t y){
    return *(image->data + x + y * image->width);
}
