#include <stdio.h>

#include "bmp_io.h"
#include "image.h"
#include "io.h"
#include "transformation.h"

int main( int argc, char** argv ) {
    if (argc != 3) {
        fprintf(stderr, "Используйте ./image-transformer <Входной файл> <Выходной файл>\n");
        return 1;
    }
    FILE* in = NULL;
    FILE* out = NULL;
    struct image input_image = {0};
    if (!open_file(argv[1], &in, "rb")){
        return 1;
    }
    if (!open_file(argv[2], &out, "wb")){
        close_file(in);
        return 1;
    }
    if (from_bmp(in, &input_image)) {
        close_file(in);
        return 1;
    }
    close_file(in);
    struct image output_image = transform(input_image);
    free_image(&input_image);
    if (to_bmp(out, &output_image)) {
        free_image(&output_image);
        close_file(out);
        return 1;
    }
    free_image(&output_image);
    close_file(out);

    return 0;
}
