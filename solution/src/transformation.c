#include "transformation.h"

#include "image.h"

struct image transform( struct image const source ) {
    struct image transformed = init_image(source.height, source.width);
    for (int32_t i = 0; i < source.height; i++) {
        for (int j = 0; j < source.width; j++) {
            set_pixel(&transformed, get_pixel(&source, j, source.height - i - 1),
                                     i, j);
        }
    }
    return transformed;
}
