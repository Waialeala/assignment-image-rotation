#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>

bool open_file (const char* filename, FILE** file, const char* modes);

bool close_file (FILE* file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
