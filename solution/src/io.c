#include "io.h"

#include <stdbool.h>
#include <stdio.h>

bool open_file (const char* filename, FILE** file, const char* modes){
    if (filename) {
        *file = fopen(filename, modes);
        if (*file) {
            return true;
        }
    }
    return false;
}

bool close_file (FILE* file){
    if (file) {
        fclose(file);
        return true;
    }
    return false;
}
