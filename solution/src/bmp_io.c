#include "bmp_io.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "image.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

static bool signature_validate(const struct bmp_header header) {
    return header.bfType == 0x4D42;
}

static bool padding_need(uint32_t width) {
    return (width * 3) % 4;
}

static uint32_t padding_calculate(uint32_t width) {
    if (padding_need(width)) {
        return 4 - ((width * 3) % 4);
    } else {
        return 0;
    }
}

static bool padding_write(FILE* file, uint32_t width) {
    int8_t zero = 0;
    for (size_t j = 0; j < padding_calculate(width); j++){
        if(!fwrite(&zero, sizeof (int8_t), 1, file)) return false;
    }
    return true;
}

static bool make_image ( FILE* file, const struct bmp_header header, struct image* image) {
    if (header.biHeight <= 0 || header.biWidth <= 0) return false;
    *image = init_image(header.biWidth, header.biHeight);
    fseek(file, header.bOffBits, SEEK_SET);
    for (int32_t i = 0; i < image->height; i++) {
        for (int32_t j = 0; j < image->width; j++) {
            struct pixel pixel = {0};
            if (!fread(&pixel, sizeof (struct pixel), 1, file)) {
                free_image(image);
                return false;
            }
            set_pixel(image, pixel, j, i);
        }
        if (padding_need(image->width)) {
            fseek(file, padding_calculate(image->width), SEEK_CUR);
        }
    }
    return true;
}

static struct bmp_header make_header(const struct image* image) {
    struct bmp_header header = {0};
    header.bfType = 0x4D42;
    header.bfileSize = sizeof (struct bmp_header)
            + image->width * image->height * sizeof(struct pixel)
            + image->height * padding_calculate(image->width);
    header.bOffBits = 54;
    header.biSize = 40;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biSizeImage = header.bfileSize - 54;
    return header;
}

enum read_status from_bmp( FILE* file, struct image* image ) {
    if (!file) return READ_FROM_NULL_FILE;
    struct bmp_header header = {0};
    if (!fread(&header, sizeof(struct bmp_header), 1, file)) return READ_INVALID_HEADER;
    if (!signature_validate(header)) return READ_INVALID_SIGNATURE;
    if (!make_image(file, header, image)) return READ_INVALID_BITS;
    return READ_OK;
}

enum write_status to_bmp( FILE* file, struct image const* image ) {
    struct bmp_header header = make_header(image);
    if (!fwrite(&header, sizeof (struct bmp_header), 1, file)) return WRITE_ERROR;
    for (int32_t i = 0; i < image->height; i++){
        for (int32_t j = 0; j < image->width; j++) {
            struct pixel pixel = get_pixel(image, j, i);
            if (!fwrite(&pixel, sizeof (struct pixel), 1, file)) return WRITE_ERROR;
        }
        if(!padding_write(file, image->width)) return WRITE_ERROR;
    }
    return WRITE_OK;
}
