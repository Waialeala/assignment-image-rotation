#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <inttypes.h>
#include <stdbool.h>

#include "pixel.h"

struct image {
    uint32_t width, height;
    struct pixel* data;
};

struct image init_image (uint32_t width, uint32_t height);

void free_image (struct image* image);

bool set_pixel (struct image* image, struct pixel pixel, uint32_t x, uint32_t y);

struct pixel get_pixel (const struct image* image, uint32_t x, uint32_t y);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
