#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H



#include "image.h"

struct image transform( struct image const source );

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATION_H
